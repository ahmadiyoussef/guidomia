package com.example.guidomia.repository


import com.example.guidomia.data.local.CarLocalDataSource
import com.example.guidomia.models.Car
import com.example.guidomia.data.remote.ReadJsonFile
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CarRepository @Inject constructor(
    private val readJsonFile: ReadJsonFile,
    private val localDataSource: CarLocalDataSource
)  {

    suspend fun getCarList(): List<Car> {
        val localCars = localDataSource.getCarList()
        if (localCars.isNotEmpty()) return localCars
        val remoteCars = readJsonFile.getCarList()
        localDataSource.saveCars(remoteCars)
        return localDataSource.getCarList()
    }

    }

