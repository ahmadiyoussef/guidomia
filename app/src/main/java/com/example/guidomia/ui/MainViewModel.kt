package com.example.guidomia.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.guidomia.models.CarModel
import com.example.guidomia.repository.CarRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: CarRepository
) : ViewModel() {

    private val _cars = MutableLiveData<List<CarModel>>()
    val cars: LiveData<List<CarModel>> get() = _cars

    private val _carMakes = MutableLiveData<List<String>>()
    val carMakes: LiveData<List<String>> get() = _carMakes

    private val _makeModels = MutableLiveData<List<String>>()
    val makeModels: LiveData<List<String>> get() = _makeModels


    private var allCars = emptyList<CarModel>()

    private val modelsByMake = mutableMapOf<String, List<String>>()

    init {
        _carMakes.value = listOf(ANY_MAKE)
        _makeModels.value = listOf(ANY_MODEL)
        prepareUiCars()
    }


    fun onClickCar(car: CarModel) {
        allCars = allCars.map { it.copy(isShowedDetail = car.id == it.id) }
        _cars.value = cars.value?.map { uiModel ->
            uiModel.copy(isShowedDetail = car.id == uiModel.id)
        }
    }

    fun onSelectMake(position: Int) {
        val make = _carMakes.value.orEmpty().getOrNull(position) ?: return

        val models = modelsByMake[make]
        _makeModels.value = when {
            models.isNullOrEmpty() -> listOf(ANY_MODEL)
            else -> models
        }

        _cars.value = when (make) {
            ANY_MAKE -> allCars
            else -> allCars.filter { it.make == make }
        }
    }

    fun onSelectModel(position: Int, selectedMakePosition: Int) {
        // Find selected model
        val model = _makeModels.value.orEmpty().getOrNull(position) ?: return
        val make = _carMakes.value.orEmpty().getOrNull(selectedMakePosition) ?: return

        val makeCars = when (make) {
            ANY_MAKE -> allCars
            else -> allCars.filter { it.make == make }
        }

        val modelCars = when (model) {
            ANY_MODEL -> makeCars
            else -> makeCars.filter { it.name == model }
        }

        _cars.value = modelCars
    }

    private fun prepareUiCars() = viewModelScope.launch {
        val carList = repository.getCarList()
        val carUiModels = carList.mapIndexed { index, car ->
            CarModel(car = car, isShowed = index == 0)
        }
        allCars = carUiModels
        _cars.value = carUiModels
        initFilters()

    }

    private fun initFilters() {
        // Group by make
        val allMaks = allCars.groupBy { it.make }

        // Order of insertion will be preserved (Linked Hash map)
        modelsByMake[ANY_MAKE] = listOf(ANY_MODEL)
        allMaks.forEach { entry ->
            val make = entry.key
            val models = entry.value.map { it.name }.distinct().toMutableList()
            models.add(0, ANY_MODEL)
            modelsByMake[make] = models
        }


        _carMakes.value = modelsByMake.keys.toList()

        _makeModels.value = listOf(ANY_MODEL)
    }

    companion object {
        const val ANY_MAKE = "Any make"
        const val ANY_MODEL = "Any model"
    }
}