package com.example.guidomia.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import ca.bell.guidomia.common.viewModel
import com.example.guidomia.R
import com.example.guidomia.adapters.CarAdapter
import com.example.guidomia.adapters.SpinnerAdapter
import com.example.guidomia.databinding.ActivityMainBinding
import com.example.guidomia.di.injector
import com.example.guidomia.tools.addDivider
import com.example.guidomia.tools.doOnItemSelected
import com.example.guidomia.tools.observe
import com.example.guidomia.tools.viewBinding
import dagger.Module



class MainActivity : AppCompatActivity() {
    private val binding by viewBinding(ActivityMainBinding::inflate)
    private val viewModel by viewModel {injector.mainViewModel}


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
            initView()
            observeViewModel()
        }
        override fun onCreateOptionsMenu(menu: Menu?): Boolean {
            menuInflater.inflate(R.menu.menu, menu)
            return true
        }

        override fun onOptionsItemSelected(item: MenuItem): Boolean {
            return super.onOptionsItemSelected(item)
        }

        private fun initView() = with(binding) {
            recyclerView.adapter = CarAdapter(onClickCar = { car ->
                viewModel.onClickCar(car)
            })
            recyclerView.addDivider(showLast = false)

            spMake.doOnItemSelected { position ->
                viewModel.onSelectMake(position)
            }

            spModel.doOnItemSelected { position ->
                val currentMakePosition = spMake.selectedItemPosition
                viewModel.onSelectModel(position, currentMakePosition)
            }
        }

        private fun observeViewModel() {

            observe(viewModel.cars) { cars ->
                val adapter = binding.recyclerView.adapter as CarAdapter
                adapter.submitList(cars)
            }

            observe(viewModel.carMakes) { makes ->
                binding.spMake.adapter = SpinnerAdapter(this, makes)
            }

            observe(viewModel.makeModels) { models ->
                binding.spModel.adapter = SpinnerAdapter(this, models)
            }


        }





}


