package com.example.guidomia.di

import android.content.Context
import com.example.guidomia.data.local.AppDatabase
import com.example.guidomia.data.local.CarDao
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    fun provideCarDao(db: AppDatabase): CarDao {
        return db.carDao()
    }

    @Provides
    @Singleton
    fun provideDataBase(appContext: Context): AppDatabase {
        return AppDatabase(appContext)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }
}