package com.example.guidomia.models

import androidx.annotation.DrawableRes
import java.util.*

data class CarModel(
    val id: String,
    val name: String,
    val make: String,
    val price: Int,
    val rating: Int,
    @DrawableRes val image: Int,
    val prosList: List<String>,
    val consList: List<String>,
    val isShowedDetail: Boolean
) {
    companion object {
        operator fun invoke(car: Car, isShowed: Boolean) = CarModel(
            id = UUID.randomUUID().toString(),
            name = car.model,
            make = car.make,
            price = car.marketPrice.toInt() / 1000,
            rating = car.rating,
            image = car.image,
            prosList = car.prosList,
            consList = car.consList,
            isShowedDetail = isShowed
        )
    }
}