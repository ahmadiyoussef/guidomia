package com.example.guidomia.adapters

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.isVisible
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ca.bell.guidomia.common.inflater

import com.example.guidomia.R
import com.example.guidomia.databinding.CarItemBinding
import com.example.guidomia.models.CarModel
import com.example.guidomia.tools.color
import com.example.guidomia.tools.dp2px


class CarAdapter(
    private val onClickCar: (CarModel) -> Unit
) : RecyclerView.Adapter<CarAdapter.ViewHolder>() {

    private val differ = AsyncListDiffer(this, CarDiffUtil())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarAdapter.ViewHolder {
        val binding = CarItemBinding.inflate(parent.inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val car = differ.currentList[position]
        holder.bind(car)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        val payload = payloads.getOrNull(0)
        if (payload is CarItemToggled) {
            val expanded = payload.expanded
            holder.toggleDetailsView(expanded)
            return
        }
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun getItemCount() = differ.currentList.size

    fun submitList(newList: List<CarModel>, callback: () -> Unit = {}) {
        differ.submitList(newList, callback)
    }

    inner class ViewHolder(
        private val binding: CarItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(carUiModel: CarModel) = with(binding) {
            ivCar.setImageResource(carUiModel.image)
            tvCarName.text = carUiModel.name
            tvCarPrice.text =
                itemView.context.getString(R.string.car_price, carUiModel.price)
            rating.rating = carUiModel.rating.toFloat()

            // Draw cons and Pros items
            pros.removeAllViews()
            cons.removeAllViews()

            val prosList = carUiModel.prosList
            val consList = carUiModel.consList

            prosList.forEach { pro ->
                pros.addView(bulletTextView(pro))
            }

            consList.forEach { con ->
                cons.addView(bulletTextView(con))
            }

            prosGroup.isVisible = prosList.isNotEmpty()
            consGroup.isVisible = consList.isNotEmpty()

            // Expanded state
            detail.isVisible = carUiModel.isShowedDetail

            itemView.setOnClickListener {
                onClickCar(carUiModel)
            }
        }

        fun toggleDetailsView(expanded: Boolean) {
            binding.detail.isVisible = expanded
        }

        private fun bulletTextView(text: String): TextView {
            val textView = TextView(itemView.context)
            textView.text = text
            textView.setTextColor(itemView.context.color(R.color.black))
            textView.setTypeface(textView.typeface, Typeface.BOLD)
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(
                R.drawable.bullet,
                0,
                0,
                0
            )
            textView.compoundDrawablePadding = dp2px(10)
            return textView
        }
    }
}