package com.example.guidomia.adapters


import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.guidomia.databinding.SpinnerDropdownBinding
import com.example.guidomia.databinding.SpinnerItemBinding


class SpinnerAdapter(
    context: Context,
    private val values: List<String>
) : ArrayAdapter<String>(context, 0, values) {

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SpinnerItemBinding.inflate(inflater, parent, false)
        val txtDisplayedValue = binding.tv
        txtDisplayedValue.text = values[position]
        return binding.root
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SpinnerDropdownBinding.inflate(inflater, parent, false)
        val txtDisplayedValue = binding.tvDisplayedValue
        txtDisplayedValue.text = values[position]
        return binding.root
    }
}