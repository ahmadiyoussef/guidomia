package com.example.guidomia.adapters

import androidx.recyclerview.widget.DiffUtil
import com.example.guidomia.models.CarModel

class CarDiffUtil : DiffUtil.ItemCallback<CarModel>() {

    override fun areItemsTheSame(oldItem: CarModel, newItem: CarModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: CarModel, newItem: CarModel): Boolean {
        return newItem == oldItem
    }

    override fun getChangePayload(oldItem: CarModel, newItem: CarModel): Any? {
        if (oldItem.id == newItem.id && oldItem.isShowedDetail != newItem.isShowedDetail) {
            // Only expanded state is changed
            return CarItemToggled(newItem.isShowedDetail)
        }
        return super.getChangePayload(oldItem, newItem)
    }
}


data class CarItemToggled(
    val expanded: Boolean
)