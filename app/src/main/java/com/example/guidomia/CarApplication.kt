package com.example.guidomia

import android.app.Application
import com.example.guidomia.di.DaggerAppComponent

class CarApplication : Application() {

    val appComponent by lazy { DaggerAppComponent.factory().create(this) }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }

    companion object {
        private lateinit var INSTANCE: CarApplication

        fun get() = INSTANCE
    }
}