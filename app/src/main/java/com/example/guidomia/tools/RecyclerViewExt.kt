package com.example.guidomia.tools

import androidx.annotation.ColorRes
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.guidomia.R
import com.example.guidomia.tools.NoLastDividerItemDecorator
import com.example.guidomia.tools.color
import com.example.guidomia.tools.dp2px
import com.example.guidomia.tools.drawable


fun RecyclerView.addDivider(
    orientation: Int = LinearLayoutManager.VERTICAL,
    @ColorRes dividerColor: Int = R.color.orange,
    showLast: Boolean = true
) {
    val drawable = context.drawable(R.drawable.divider)
    drawable?.setTint(context.color(dividerColor))
    if (drawable == null) return
    if (showLast) {
        val dividerItemDecoration = DividerItemDecoration(context, orientation)
        dividerItemDecoration.setDrawable(drawable)
        addItemDecoration(dividerItemDecoration)
    } else {
        val dividerItemDecoration = NoLastDividerItemDecorator(
            divider = drawable,
            context = context,
            orientation = orientation
        )
        dividerItemDecoration.setDrawable(drawable)
        addItemDecoration(dividerItemDecoration)
    }
}

fun RecyclerView.ViewHolder.dp2px(dp: Int): Int {
    return itemView.context.dp2px(dp)
}